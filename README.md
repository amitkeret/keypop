
# KeyPop
> Shortcut list popup for keyboard-shortcut libraries

This plugin will automagically build a legend-syle HTML list of your defined keyboard shortcuts.
The generated markup is *very* comprehensive, wrapping every element in the key-combos
(keystrokes, operators, modifiers, symbols...) with a unique tag. This allows for some
pretty awesome styling

**Supported libraries:** [Mousetrap](//craig.is/killing/mice), [jwerty](//keithamus.github.io/jwerty), [keyboardJS](//github.com/RobertWHurst/KeyboardJS), [Kibo](//github.com/marquete/kibo), [keymaster](//github.com/madrobby/keymaster), [hotkeys](//jaywcjlove.github.io/hotkeys)
(suggest another?)

## Installation
1. Include your keyboard-binding library of choice
2. *optional:* Include [picoModal](//github.com/Nycto/PicoModal) to enable popup-behavior
3. Include **KeyPop** and one of the CSS themes
```html
<script src="path/to/keyboard-binding-lib.min.js"></script>
<script src="path/to/picoModal.js"></script> <!-- optional -->
<script src="path/to/KeyPop.min.js"></script>
<link rel="stylesheet" href="path/to/KeyPop.theme.min.css" />
```

**Demos:** [Mousetrap](demo/mousetrap.html), [jwerty](demo/jwerty.html), [keyboardJS](demo/keyboardjs.html), [Kibo](demo/kibo.html), [keymaster](demo/keymaster.html), [hotkeys](demo/hotkeys.html)

## Usage

Invoke your library's keyboard-binding function in the same way. The only difference is in the
function's signature - the second argument is now the **combo description**, which will be used
in the shortcut-list.
```javascript
bindingFunction('r', 'Refresh', function(){ window.location.reload(); });
```

Pass an *empty* combo to the binding function, and it will be treated as a **group header**.
In this way you can group your shortcuts into categories for better readability.
```javascript
bindingFunction('', 'Navigation' }); // "Navigation" category
bindingFunction('r', 'Refresh', function(){ window.location.reload(); });
bindingFunction('b', 'Back', function(){ window.history.back(); });
...
```

#### KeyPop.*get( [modal] )*
KeyPop is attached to your library's class as `[class].KeyPop`.  
*Note: If your binding library doesn't create its own class, it will be attached to `window.KeyPop`.*

**KeyPop.get** generates an HTML legend for your shortcuts, and sends it to picoModal.
```javascript
[class].KeyPop.get(); // picoModal fired!
```

If you haven't included picoModal, you will get the raw HTML.  
Passing `false` to `KeyPop.get` would acheive the same result.
```javascript
var legend = [class].KeyPop.get(); // If you have't included picoModal, or:
var legend = [class].KeyPop.get( false );
// Use with another modal library, eg. Bootstrap.
$(legend).modal();
```

## License
MIT
