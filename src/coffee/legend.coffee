Variables =   require './vars'
Data =        require './data'

# HTML formatted representation of Data
legend = null

# Determine the class/es for a key
# Each key will have its unique class, plus other "group" classes
# According to the Variables.keys multi-array
collectClass = (key)->
  if key is '' then return ''
  attrs = [ pref(key) ]
  attrs.push pref type for type in ['modifier', 'symbol'] when key in Variables.keys["#{type}s"]
  attrs.join ' '

pref = (v)-> "#{Variables['nameLC']}-#{v}"

# Build an HTML tag
tag = (tag, classes = '', text = '', attrs = {})->
  attr = if Object.keys(attrs).length then " #{atr}='#{val}'" for atr, val of attrs else ''
  classs = if classes is '' then '' else " class='#{collectClass(classes)}'"
  res = "<#{tag}#{classs}#{attr}>#{text}</#{tag}>"
  res

# Build the HTML tree for the legend
buildLegend = ->
  trs = []
  for row in do Data.getData
    if row.type == 'group'
      td1 = tag 'div', row.code, row.description
      td1 = tag 'td', 'group', td1, {colspan: 2}
      td2 = ''
    else
      seqs = []
      for sequence, si in row.code
        comb = []
        for combo, ci in sequence
          kbds = []
          for key, ki in combo
            kbds.push tag Variables.parts.key.tag, key, tag Variables.parts.text.tag, Variables.parts.text.class, key
          kbds = kbds.join tag 'span', Variables.parts.key.sep
          comb.push tag Variables.parts.combo.tag, Variables.parts.combo.class, kbds
        comb = comb.join tag 'span', Variables.parts.combo.sep
        seqs.push tag Variables.parts.sequence.tag, Variables.parts.sequence.class, comb
      seqs = seqs.join tag 'span', Variables.parts.sequence.sep
      td1 = tag Variables.parts.main.tag, Variables.parts.main.class, seqs
      td1 = tag 'td', 'wrap', td1
      td2 = tag 'td', 'description', row.description
    trs.push tag 'tr', '', (td1 + td2)
  trs = trs.join "\n"
  table = tag 'table', '', trs
  res = [
    tag 'div', '', Variables.modalAttrs.title.text, {id: Variables.modalAttrs.title.id}
    tag 'div', '', table, {id: Variables.modalAttrs.list}
  ]
  res = tag 'div', '', res.join("\n"), {id: Variables.modalAttrs.body}

Legend =
  getLegend: ->
    legend = do buildLegend if legend is null
    legend

module.exports = Legend
