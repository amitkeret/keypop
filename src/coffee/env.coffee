libs = require '../../supported-libs.json'

config = do ->
  for libname, lib of libs
    if lib.className and lib.className of window then return lib
    if not lib.className and lib.bindFunc.names[0] of window then return lib
  no

if config is off
  return no

# Detect the environment we're working under
# Create environment-specific binding function to override the original
_class = if config.className then window[config.className] else window
_proto = config.className and config.bindFunc.isPrototype

_bind = do ->
  if _proto
    ret = (_class::[func] for func in config.bindFunc.names)
  else
    ret = (_class[func] for func in config.bindFunc.names)
  ret

Environment =
  config: config
  bind:  (newBind)->
    for func, i in config.bindFunc.names
      old = _bind[i]
      if _proto
        _class::["#{func}_old"] = old
        _class::[func] = ->
          args = newBind arguments
          return if args is no
          @["#{func}_old"].apply this, args
      else
        _class[func] = ->
          args = newBind arguments
          return if args is no
          old.apply this, args

module.exports = Environment
