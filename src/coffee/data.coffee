Variables =     require './vars'
Environment =   require './env'

# JS object representation of Variables.list
data = null

# Separate plain group items from items containing key combinations
parseBindItem = (bindItem)->
  [code, description] = bindItem
  row = {description: description}
  if code.indexOf('group') isnt -1
    row.type = 'group'
    row.code = code
  else row.code = parseCode code
  row

# Create a multi-dimentional array representing Mousetrap-code:
# [         Main array  will be wrapped with ".code"
#   [       Sequence    will be wrapped with ".sequence",  separated by ".separator"
#     [     Combo       will be wrapped with ".combo",     separated by ".sequencer"
#       [   Key         will be wrapped with "<kbd>",      separated by ".combinator"
#           Text        will be wrapped with "text"
parseCode = (code)->
  _delim = Environment.config.delim
  # BUGFIX: not handling arrays very well in keyboardJS
  #         REASON: the original bind function calls itself if array is met
  #                 and so... the new bind function is called again
  #         FIX(?): keep array of individual combos and compare
  #                 whenever a new combo attempts to be added ??
  codes = if code instanceof Array then code[..] else code.split parseCodeRegex _delim.multi
  # Turning:
  #   sequence string   "g ctrl+a"
  #   into combos array ["g", "ctrl+a"]
  codes[i] = seq.split parseCodeRegex _delim.sequence for seq, i in codes
  codes[i][j] = key.split parseCodeRegex _delim.combo for key, j in combos for combos, i in codes
  codes

# Create a RegExp for the split, to account for spaces around delimiters.
# BUGFIX: does not account for cases when the delimiter is used as a character.
#         eg.: in jwerty, to bind "?", need to bind "shift+/",
#         but the "/" char is the delimiter for multiple combos
parseCodeRegex = (delim)->
  return off if delim is off
  # if the delimiter is a literal space, no point in regex'ing for spaces...
  return delim if delim is ' '
  new RegExp("\\ \?\\#{delim}\\ \?")

Data =
  getData: ->
    data = (parseBindItem bindItem for bindItem in Variables.list) if data is null
    data

module.exports = Data
