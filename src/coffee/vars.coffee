name = 'KeyPop'
nameLC = do name.toLowerCase

list = []

# Various HTML values for _buildLegend() etc.
parts =
  main:
    tag: 'span'
    class: 'code'
    sep: ''
  sequence:
    tag: 'span'
    class: 'sequence'
    sep: 'separator'
  combo:
    tag: 'span'
    class: 'combo'
    sep: 'sequencer'
  key:
    tag: 'kbd'
    class: ''
    sep: 'combinator'
  text:
    tag: 'span'
    class: 'text'
    sep: ''

# Various values for picoModal and HTML
modalAttrs =
  overlay:  "#{nameLC}-modal-overlay"
  modal:    "#{nameLC}-modal-main"
  title:
    text:   'Keyboard shortcuts'
    id:     "#{nameLC}-modal-title"
  body:     "#{nameLC}-modal-content"
  list:     "#{nameLC}-modal-list"
  close:    "#{nameLC}-modal-close-btn"

# Key separation into groups
# This will determine what classes each key is wrapped with
keys =
  modifiers: [
    'shift',
    'ctrl'
    'alt'
    'meta'
    'option'
    'command'
    'mod'
  ]
  special: [
    'backspace'
    'tab'
    'enter'
    'return'
    'capslock'
    'esc'
    'escape'
    'space'
    'pageup'
    'pagedown'
    'end'
    'home'
    'left'
    'up'
    'right'
    'down'
    'ins'
    'del'
    'plus'
  ]
keys.symbols = [].concat [keys.modifiers, keys.special]...

module.exports =
  name:       name
  nameLC:     nameLC
  list:       list
  parts:      parts
  modalAttrs: modalAttrs
  keys:       keys
