# Environment (library) detection
Environment = require './env'
if Environment is off
  throw new Error 'No library or library not supported'
  return

Variables =   require './vars'
Data =        require './data'
Legend =      require './legend'
Modal =       require './modal'

# Add a simple (no shortcut) group to the list
_bindGroupNum = 0
addBindGroup = (group)->
  Variables.list.push ["group#{_bindGroupNum}", group]
  _bindGroupNum++

# Create the binding call "hook" to populate the shortcut list
newBind = (args)->
  # Support native call:
  # We expect the old callbackIndex to hold a different argument
  # (probably our description) since all arguments were pushed.
  # - If it's still the callback, means this is a legacy call
  #   so we'll use the combo string for the description.
  # - If there is a description, we'll store it and then remove it
  #   before sending the arguments to the native function.
  args = Array::slice.call args, 0
  callbackIndex = Environment.config.bindFunc.callbackIndex
  if args[callbackIndex] instanceof Function
    combo = description = args[callbackIndex - 1]
  else
    combo = args[callbackIndex - 1]
    description = args[callbackIndex]
    args = args[...callbackIndex].concat args[callbackIndex+1..]

  if combo is ''
    addBindGroup description
    return no
  else
    Variables.list.push [combo, description]
    return args

# Finally, override the native shortcut-binding call
Environment.bind newBind

append =
  get: (modal = on)->
    legend = do Legend.getLegend
    # If picoModal was not loaded, will return HTML-formatted string
    if not window.picoModal? or modal is off then return legend
    else Modal.show legend

# Append the shortcut-list functionality
if Environment.config.className and window[Environment.config.className]?
  window[Environment.config.className][Variables.name] = append
else module.exports = append
