Variables =   require './vars'
Legend =      require './legend'

# Will hold the modal object
modal = null

Modal =
  show: (legend)->
    if modal is null
      modal = window.picoModal
        content: do Legend.getLegend
        overlayClass: Variables.modalAttrs.overlay
        modalClass:   Variables.modalAttrs.modal
        closeClass:   Variables.modalAttrs.close

    modal.show()

module.exports = Modal
