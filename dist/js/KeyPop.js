/**
 *                 ____
 *  _  __         |  _ \ ___  _ __
 * | |/ /___ _  _ | |_) / _ \| '_ \
 * | ' </ -_) || ||  __/ (_) | |_) |
 * |_|\_\___|\_, ||_|   \___/| .__/
 *           |__/            |_|
 * Shortcut list popup for keyboard-shortcut libraries
 *
 * @version 0.4.0
 * @author Amit Keret
 * @license MIT
 * @url 
 */
(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.KeyPop = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var Data, Environment, Legend, Modal, Variables, _bindGroupNum, addBindGroup, append, newBind;

Environment = require('./env');

if (Environment === false) {
  throw new Error('No library or library not supported');
  return;
}

Variables = require('./vars');

Data = require('./data');

Legend = require('./legend');

Modal = require('./modal');

_bindGroupNum = 0;

addBindGroup = function(group) {
  Variables.list.push(["group" + _bindGroupNum, group]);
  return _bindGroupNum++;
};

newBind = function(args) {
  var callbackIndex, combo, description;
  args = Array.prototype.slice.call(args, 0);
  callbackIndex = Environment.config.bindFunc.callbackIndex;
  if (args[callbackIndex] instanceof Function) {
    combo = description = args[callbackIndex - 1];
  } else {
    combo = args[callbackIndex - 1];
    description = args[callbackIndex];
    args = args.slice(0, callbackIndex).concat(args.slice(callbackIndex + 1));
  }
  if (combo === '') {
    addBindGroup(description);
    return false;
  } else {
    Variables.list.push([combo, description]);
    return args;
  }
};

Environment.bind(newBind);

append = {
  get: function(modal) {
    var legend;
    if (modal == null) {
      modal = true;
    }
    legend = Legend.getLegend();
    if ((window.picoModal == null) || modal === false) {
      return legend;
    } else {
      return Modal.show(legend);
    }
  }
};

if (Environment.config.className && (window[Environment.config.className] != null)) {
  window[Environment.config.className][Variables.name] = append;
} else {
  module.exports = append;
}


},{"./data":2,"./env":3,"./legend":4,"./modal":5,"./vars":6}],2:[function(require,module,exports){
var Data, Environment, Variables, data, parseBindItem, parseCode, parseCodeRegex;

Variables = require('./vars');

Environment = require('./env');

data = null;

parseBindItem = function(bindItem) {
  var code, description, row;
  code = bindItem[0], description = bindItem[1];
  row = {
    description: description
  };
  if (code.indexOf('group') !== -1) {
    row.type = 'group';
    row.code = code;
  } else {
    row.code = parseCode(code);
  }
  return row;
};

parseCode = function(code) {
  var _delim, codes, combos, i, j, k, key, l, len, len1, len2, m, seq;
  _delim = Environment.config.delim;
  codes = code instanceof Array ? code.slice(0) : code.split(parseCodeRegex(_delim.multi));
  for (i = k = 0, len = codes.length; k < len; i = ++k) {
    seq = codes[i];
    codes[i] = seq.split(parseCodeRegex(_delim.sequence));
  }
  for (i = l = 0, len1 = codes.length; l < len1; i = ++l) {
    combos = codes[i];
    for (j = m = 0, len2 = combos.length; m < len2; j = ++m) {
      key = combos[j];
      codes[i][j] = key.split(parseCodeRegex(_delim.combo));
    }
  }
  return codes;
};

parseCodeRegex = function(delim) {
  if (delim === false) {
    return false;
  }
  if (delim === ' ') {
    return delim;
  }
  return new RegExp("\\ \?\\" + delim + "\\ \?");
};

Data = {
  getData: function() {
    var bindItem;
    if (data === null) {
      data = (function() {
        var k, len, ref, results;
        ref = Variables.list;
        results = [];
        for (k = 0, len = ref.length; k < len; k++) {
          bindItem = ref[k];
          results.push(parseBindItem(bindItem));
        }
        return results;
      })();
    }
    return data;
  }
};

module.exports = Data;


},{"./env":3,"./vars":6}],3:[function(require,module,exports){
var Environment, _bind, _class, _proto, config, libs;

libs = require('../../supported-libs.json');

config = (function() {
  var lib, libname;
  for (libname in libs) {
    lib = libs[libname];
    if (lib.className && lib.className in window) {
      return lib;
    }
    if (!lib.className && lib.bindFunc.names[0] in window) {
      return lib;
    }
  }
  return false;
})();

if (config === false) {
  return false;
}

_class = config.className ? window[config.className] : window;

_proto = config.className && config.bindFunc.isPrototype;

_bind = (function() {
  var func, ret;
  if (_proto) {
    ret = (function() {
      var j, len, ref, results;
      ref = config.bindFunc.names;
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        func = ref[j];
        results.push(_class.prototype[func]);
      }
      return results;
    })();
  } else {
    ret = (function() {
      var j, len, ref, results;
      ref = config.bindFunc.names;
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        func = ref[j];
        results.push(_class[func]);
      }
      return results;
    })();
  }
  return ret;
})();

Environment = {
  config: config,
  bind: function(newBind) {
    var func, i, j, len, old, ref, results;
    ref = config.bindFunc.names;
    results = [];
    for (i = j = 0, len = ref.length; j < len; i = ++j) {
      func = ref[i];
      old = _bind[i];
      if (_proto) {
        _class.prototype[func + "_old"] = old;
        results.push(_class.prototype[func] = function() {
          var args;
          args = newBind(arguments);
          if (args === false) {
            return;
          }
          return this[func + "_old"].apply(this, args);
        });
      } else {
        results.push(_class[func] = function() {
          var args;
          args = newBind(arguments);
          if (args === false) {
            return;
          }
          return old.apply(this, args);
        });
      }
    }
    return results;
  }
};

module.exports = Environment;


},{"../../supported-libs.json":7}],4:[function(require,module,exports){
var Data, Legend, Variables, buildLegend, collectClass, legend, pref, tag,
  indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

Variables = require('./vars');

Data = require('./data');

legend = null;

collectClass = function(key) {
  var attrs, i, len, ref, type;
  if (key === '') {
    return '';
  }
  attrs = [pref(key)];
  ref = ['modifier', 'symbol'];
  for (i = 0, len = ref.length; i < len; i++) {
    type = ref[i];
    if (indexOf.call(Variables.keys[type + "s"], key) >= 0) {
      attrs.push(pref(type));
    }
  }
  return attrs.join(' ');
};

pref = function(v) {
  return Variables['nameLC'] + "-" + v;
};

tag = function(tag, classes, text, attrs) {
  var atr, attr, classs, res, val;
  if (classes == null) {
    classes = '';
  }
  if (text == null) {
    text = '';
  }
  if (attrs == null) {
    attrs = {};
  }
  attr = (function() {
    var results;
    if (Object.keys(attrs).length) {
      results = [];
      for (atr in attrs) {
        val = attrs[atr];
        results.push(" " + atr + "='" + val + "'");
      }
      return results;
    } else {
      return '';
    }
  })();
  classs = classes === '' ? '' : " class='" + (collectClass(classes)) + "'";
  res = "<" + tag + classs + attr + ">" + text + "</" + tag + ">";
  return res;
};

buildLegend = function() {
  var ci, comb, combo, i, j, k, kbds, key, ki, l, len, len1, len2, len3, ref, ref1, res, row, seqs, sequence, si, table, td1, td2, trs;
  trs = [];
  ref = Data.getData();
  for (i = 0, len = ref.length; i < len; i++) {
    row = ref[i];
    if (row.type === 'group') {
      td1 = tag('div', row.code, row.description);
      td1 = tag('td', 'group', td1, {
        colspan: 2
      });
      td2 = '';
    } else {
      seqs = [];
      ref1 = row.code;
      for (si = j = 0, len1 = ref1.length; j < len1; si = ++j) {
        sequence = ref1[si];
        comb = [];
        for (ci = k = 0, len2 = sequence.length; k < len2; ci = ++k) {
          combo = sequence[ci];
          kbds = [];
          for (ki = l = 0, len3 = combo.length; l < len3; ki = ++l) {
            key = combo[ki];
            kbds.push(tag(Variables.parts.key.tag, key, tag(Variables.parts.text.tag, Variables.parts.text["class"], key)));
          }
          kbds = kbds.join(tag('span', Variables.parts.key.sep));
          comb.push(tag(Variables.parts.combo.tag, Variables.parts.combo["class"], kbds));
        }
        comb = comb.join(tag('span', Variables.parts.combo.sep));
        seqs.push(tag(Variables.parts.sequence.tag, Variables.parts.sequence["class"], comb));
      }
      seqs = seqs.join(tag('span', Variables.parts.sequence.sep));
      td1 = tag(Variables.parts.main.tag, Variables.parts.main["class"], seqs);
      td1 = tag('td', 'wrap', td1);
      td2 = tag('td', 'description', row.description);
    }
    trs.push(tag('tr', '', td1 + td2));
  }
  trs = trs.join("\n");
  table = tag('table', '', trs);
  res = [
    tag('div', '', Variables.modalAttrs.title.text, {
      id: Variables.modalAttrs.title.id
    }), tag('div', '', table, {
      id: Variables.modalAttrs.list
    })
  ];
  return res = tag('div', '', res.join("\n"), {
    id: Variables.modalAttrs.body
  });
};

Legend = {
  getLegend: function() {
    if (legend === null) {
      legend = buildLegend();
    }
    return legend;
  }
};

module.exports = Legend;


},{"./data":2,"./vars":6}],5:[function(require,module,exports){
var Legend, Modal, Variables, modal;

Variables = require('./vars');

Legend = require('./legend');

modal = null;

Modal = {
  show: function(legend) {
    if (modal === null) {
      modal = window.picoModal({
        content: Legend.getLegend(),
        overlayClass: Variables.modalAttrs.overlay,
        modalClass: Variables.modalAttrs.modal,
        closeClass: Variables.modalAttrs.close
      });
    }
    return modal.show();
  }
};

module.exports = Modal;


},{"./legend":4,"./vars":6}],6:[function(require,module,exports){
var keys, list, modalAttrs, name, nameLC, parts, ref;

name = 'KeyPop';

nameLC = name.toLowerCase();

list = [];

parts = {
  main: {
    tag: 'span',
    "class": 'code',
    sep: ''
  },
  sequence: {
    tag: 'span',
    "class": 'sequence',
    sep: 'separator'
  },
  combo: {
    tag: 'span',
    "class": 'combo',
    sep: 'sequencer'
  },
  key: {
    tag: 'kbd',
    "class": '',
    sep: 'combinator'
  },
  text: {
    tag: 'span',
    "class": 'text',
    sep: ''
  }
};

modalAttrs = {
  overlay: nameLC + "-modal-overlay",
  modal: nameLC + "-modal-main",
  title: {
    text: 'Keyboard shortcuts',
    id: nameLC + "-modal-title"
  },
  body: nameLC + "-modal-content",
  list: nameLC + "-modal-list",
  close: nameLC + "-modal-close-btn"
};

keys = {
  modifiers: ['shift', 'ctrl', 'alt', 'meta', 'option', 'command', 'mod'],
  special: ['backspace', 'tab', 'enter', 'return', 'capslock', 'esc', 'escape', 'space', 'pageup', 'pagedown', 'end', 'home', 'left', 'up', 'right', 'down', 'ins', 'del', 'plus']
};

keys.symbols = (ref = []).concat.apply(ref, [keys.modifiers, keys.special]);

module.exports = {
  name: name,
  nameLC: nameLC,
  list: list,
  parts: parts,
  modalAttrs: modalAttrs,
  keys: keys
};


},{}],7:[function(require,module,exports){
module.exports={
  "mousetrap": {
    "className": "Mousetrap",
    "url": "craig.is/killing/mice",
    "bindFunc": {
      "signature": "bind = function(keys, callback[, action])",
      "names": ["bind"],
      "isPrototype": true,
      "callbackIndex": 1
    },
    "delim": {
      "multi": "array",
      "sequence": " ",
      "combo": "+"
    }
  },
  "jwerty": {
    "className": "jwerty",
    "url": "keithamus.github.io/jwerty",
    "bindFunc": {
      "signature": "key: function (jwertyCode, callbackFunction, callbackContext...",
      "names": ["key"],
      "isPrototype": false,
      "callbackIndex": 1
    },
    "delim": {
      "multi": "/",
      "sequence": ",",
      "combo": "+"
    }
  },
  "keyboardjs": {
    "className": "keyboardJS",
    "url": "github.com/RobertWHurst/KeyboardJS",
    "bindFunc": {
      "signature": "bind = function(keyComboStr, pressHandler[, releaseHandler][,preventRepeatByDefault])",
      "names": ["bind"],
      "isPrototype": false,
      "callbackIndex": 1
    },
    "delim": {
      "multi": false, "BUGFIX": "combo array don't work, @see core.coffee for BUGFIX",
      "sequence": ">", "BUGFIX": "sequences don't work",
      "combo": "+"
    }
  },
  "kibo": {
    "className": "Kibo",
    "url": "github.com/marquete/kibo",
    "bindFunc": {
      "signature": "delegate = function(upOrDown, keys, func)",
      "names": ["down", "up"],
      "isPrototype": true,
      "callbackIndex": 1
    },
    "delim": {
      "multi": "array",
      "sequence": false,
      "combo": " "
    }
  },
  "keymaster": {
    "className": false,
    "url": "github.com/madrobby/keymaster",
    "bindFunc": {
      "signature": "function key(key[, scope,] method)",
      "names": ["key"],
      "isPrototype": false,
      "callbackIndex": 1
    },
    "delim": {
      "multi": ",",
      "sequence": false,
      "combo": "+"
    }
  },
  "hotkeys": {
    "className": false,
    "url": "jaywcjlove.github.io/hotkeys",
    "bindFunc": {
      "signature": "function hotkeys(key[, scope,] method)",
      "names": ["hotkeys"],
      "isPrototype": false,
      "callbackIndex": 1
    },
    "delim": {
      "multi": ",",
      "sequence": false,
      "combo": "+"
    }
  }
}

},{}]},{},[1])(1)
});