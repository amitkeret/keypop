module.exports = (grunt)->

  conf =
    grunt:
      config: 'grunt/config'
      tasks:  'grunt/tasks'

    pkg:      grunt.file.readJSON 'package.json'
    supports: grunt.file.readJSON 'supported-libs.json'

  # load the configuration object
  conf.config =
    src: "#{conf.grunt.config}/*"
  conf = require('load-grunt-configs')(grunt, conf)
  grunt.initConfig conf

  grunt.loadNpmTasks task for task in [
    'grunt-banner'
    'grunt-browserify'
    'grunt-contrib-copy'
    'grunt-contrib-jade'
    'grunt-contrib-stylus'
    'grunt-contrib-uglify'
    'grunt-contrib-watch'
    'grunt-postcss'
  ]

  tasks =
    harp:     require("./#{conf.grunt.tasks}/harp.coffee")(grunt, conf)

    dev:      ['jade:readme', 'browserify:dev', 'harp', 'watch']
    dist_js:  ['browserify:dist', 'uglify']
    dist_css: ['stylus:dist-full', 'stylus:dist-min', 'postcss:dist']
    dist:     ['dist_js', 'dist_css', 'usebanner']
    default:  ['dev']

  grunt.registerTask task, subtasks for task, subtasks of tasks
